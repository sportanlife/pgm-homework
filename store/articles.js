export const state = () => ({
  articles: []
});

export const mutations = {
  setArticles(state, articles) {
    state.articles = articles;
  }
};

export const actions = {
  async fetch({commit}) {
    const articles = await this.$axios.$get( '/editorsChoice/articles?limit=5').then(resp => resp.map(article => {
      return {
        id: article.id,
        title: article.headline,
        img: article.thumbnail.sources.landscape.large,
        imgSm: article.thumbnail.sources.portrait.medium,
        description: article.articleLead.find(item => item.wordCount > 0).html
      }
    }));
    commit('setArticles', articles);
  }
}

export const getters = {
  articles: s => s.articles
}
